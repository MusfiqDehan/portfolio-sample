// Setup
function phoneticLookup(val) {
  var result = "";

  // Only change code below this line
  var lookup = {
    alpha: "Adams",
    bravo: "Boston",
    charlie: "Chicago",
    delta: "Denver",
    echo: "Easy",
    foxtrot: "Frank"
  };
  result = lookup.val;
  // Only change code above this line
  return result;
}

console.log(phoneticLookup("charlie"));



//Checking object found or not

function checkObj(obj, checkProp) {
  // Only change code below this line
  if(obj.hasOwnProperty(checkProp))
    return obj.checkProp;
  else
    return "Not Found";
  // Only change code above this line
}

